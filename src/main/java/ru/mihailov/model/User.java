package ru.mihailov.model;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "chat_users")
public class User {

    @Id
    private String id = UUID.randomUUID().toString();

    private String name;

}
