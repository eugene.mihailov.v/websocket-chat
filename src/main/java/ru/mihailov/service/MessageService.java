package ru.mihailov.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mihailov.model.Message;
import ru.mihailov.repository.MessageRepository;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    public List<Message> findAllByUserId(String userId) {
        return messageRepository.findAllByUserId(userId);
    }

    public List<Message> findAllByRoomId(String groupId) {
        return messageRepository.findAllByRoomId(groupId);
    }

    @Transactional
    public Message saveMessage(String roomId, String userId, String text) {
        Message message = new Message();
        message.setRoomId(roomId);
        message.setUserId(userId);
        message.setText(text);
        return messageRepository.save(message);
    }

}
