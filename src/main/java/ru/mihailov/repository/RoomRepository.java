package ru.mihailov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mihailov.model.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, String> {

    Room findByName(String name);

}
