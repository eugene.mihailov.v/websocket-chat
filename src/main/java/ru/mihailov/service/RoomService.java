package ru.mihailov.service;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mihailov.model.Room;
import ru.mihailov.repository.RoomRepository;

@Service
public class RoomService {

    @Autowired
    private RoomRepository roomRepository;

    public Room findById(String id) {
        return roomRepository.findById(id).orElse(null);
    }

    public Room findByName(String name) {
        return roomRepository.findByName(name);
    }

    @Transactional
    public Room saveRoom(String name) {
        Room room = new Room();
        room.setName(name);
        return roomRepository.save(room);
    }

}
