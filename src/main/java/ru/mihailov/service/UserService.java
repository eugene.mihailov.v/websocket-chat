package ru.mihailov.service;

import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mihailov.model.User;
import ru.mihailov.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User findById(String id) {
        return userRepository.findById(id).orElse(null);
    }

    public User findByName(String name) {
        return userRepository.findByName(name);
    }

    @Transactional
    public User saveUser(String name) {
        User user = new User();
        user.setName(name);
        return userRepository.save(user);
    }

}
