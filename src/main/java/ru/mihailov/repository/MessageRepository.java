package ru.mihailov.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mihailov.model.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, String> {

    List<Message> findAllByUserId(String userId);

    List<Message> findAllByRoomId(String roomId);

}
