package ru.mihailov.controller;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import ru.mihailov.service.MessageService;
import ru.mihailov.service.RoomService;
import ru.mihailov.service.UserService;
import ru.mihailov.types.Message;

import static java.lang.String.format;

@Controller
public class ChatRoomController {

    private static final Logger logger = LoggerFactory.getLogger(ChatRoomController.class);

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private RoomService roomService;

    @Autowired
    private MessageService messageService;

    @MessageMapping("/chat/{roomId}/sendMessage")
    public void sendMessage(@DestinationVariable String roomId, @Payload Message chatMessage) {
        logger.info(roomId+" Chat message recieved is "+chatMessage.getContent());
        messagingTemplate.convertAndSend(format("/chat-room/%s", roomId), chatMessage);

        asyncSaveMessage(roomId, chatMessage.getSender(), chatMessage.getContent());
    }

    @MessageMapping("/chat/{roomId}/addUser")
    public void addUser(@DestinationVariable String roomId, @Payload Message chatMessage,
                        SimpMessageHeaderAccessor headerAccessor) {
        String currentRoomId = (String) headerAccessor.getSessionAttributes().put("room_id", roomId);
        if (currentRoomId != null) {
            Message leaveMessage = new Message();
            leaveMessage.setType(Message.MessageType.LEAVE);
            leaveMessage.setSender(chatMessage.getSender());
            messagingTemplate.convertAndSend(format("/chat-room/%s", currentRoomId), leaveMessage);
        }
        headerAccessor.getSessionAttributes().put("name", chatMessage.getSender());
        messagingTemplate.convertAndSend(format("/chat-room/%s", roomId), chatMessage);

        asyncSaveRoom(roomId);
        asyncSaveUser(chatMessage.getSender());
    }

    private void asyncSaveMessage(String roomId, String userId, String text) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            messageService.saveMessage(roomId, userId, text);
            System.out.println("async saveMessage");
        });
    }

    private void asyncSaveRoom(String roomId) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            roomService.saveRoom(roomId);
            System.out.println("async saveRoom");
        });
    }

    private void asyncSaveUser(String userId) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            userService.saveUser(userId);
            System.out.println("async saveUser");
        });
    }

}
